import createArrayMaskBin from "./createArrayMaskBin.mjs";
import findIp from "./findIp.mjs";
import {
	arrayBitwiseOperations,
	convertArrayDecToArrayBin,
	inverseZeroAndOneCharForArray,
} from "./helpers.mjs";


function main() {
	const cidr = process.argv[2];
	const { ip, diffusion, mask } = getIpAndDiffusion(cidr);
	const { nbIp, nbIpAvailable } = findNbIp(mask);
	console.log(`The ip address is ${ip}`);
	console.log(`The diffusion address is ${diffusion}`);
	console.log(`There is ${nbIp} ip address`);
	console.log(`There is ${nbIpAvailable} ip address available`);
}

/**
 * Take a Cidr (Classless Inter-Domain Routing) and return a diffusion address and a ip address
 * @param {String} cidr - 10.27.233.17/20
 * @returns {{diffusion: string, ip: (string|*)}} - Ex: {diffusion: 10.27.233.17, ip:10.27.239.255}
 */
function getIpAndDiffusion(cidr) {
	const { mask, ip } = findIp(cidr);

	if ( mask < 0 || mask > 32 ) throw new Error(
		"Mask must be between 0 and 32");

	// Convert Ip to array of string.
	const arrayIp = ip.split(".");
	arrayIp.forEach(num => {
		if ( num < 0 || num > 255 ) throw new Error(
			"Address Ip must be between 0 and 255");
	});
	// console.log("arrayIp", arrayIp);
	// console.log("\n------\n");

	const arrayIpBin = convertArrayDecToArrayBin(arrayIp);

	// console.log("arrayIpBin", arrayIpBin);
	// console.log("\n------\n");

	const arrayMaskBin = createArrayMaskBin(mask);
	// console.log("arrayMaskBin", arrayMaskBin);
	// console.log("\n------\n");

	const arrayNetworkBin = arrayBitwiseOperations(arrayIpBin,
		arrayMaskBin,
		"&&");

	// console.log("arrayNetworkBin", arrayNetworkBin);
	// console.log("\n------\n");

	const arrayMaskBinInv = inverseZeroAndOneCharForArray(arrayMaskBin);
	// console.log("arrayMaskBinInv", arrayMaskBinInv);
	// console.log("\n------\n");

	const arrayDiffusionBin = arrayBitwiseOperations(
		arrayNetworkBin,
		arrayMaskBinInv,
		"||");
	// console.log("arrayDiffusionBin", arrayDiffusionBin);
	// console.log("\n------\n");

	const arrayDiffusionDec = arrayDiffusionBin.map(s => parseInt(s, 2));
	const diffusion = arrayDiffusionDec.join(".");
	// console.log(arrayDiffusionDec, diffusion);

	return { diffusion, ip, mask };
}

function findNbIp(mask) {
	let intMask = parseInt(mask);
	let temp = 32 - intMask;
	const nbIp = 2 ** temp,
		nbIpAvailable = nbIp - 2 >= 0 ? nbIp - 2 : 0;
	return { nbIp, nbIpAvailable };
}

main();