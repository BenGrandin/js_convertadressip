/**
 * @param {String[]} arrayIpBin
 * @param {String[]} arrayMaskBinInv
 */
function findDiffusionBin(arrayIpBin, arrayMaskBinInv) {
	let diffusionBin = ["", "", "", ""];
	for ( let i = 0 ; i < 4 ; i++ ) {
		for ( let j = 0 ; j < 8 ; j++ ) {
			if ( arrayIpBin[i][j] === "1" || arrayMaskBinInv[i][j] === "1" ) {
				diffusionBin[i] = diffusionBin[i].concat("1");
			} else {
				diffusionBin[i] = diffusionBin[i].concat("0");
			}
		}
	}
	return diffusionBin;
}

export default findDiffusionBin;