/**
 * @param {string} cidr - (Classless Inter-Domain Routing) Ex: "10.27.233.17/20"
 * @returns {{ip: string | *, mask: string}} | Ex: {ip :"10.27.233.17", "20"  }
 */
function findIp(cidr) {
	const mask = /\/[0-9]*/.exec(cidr)[0].replace("/", "");
	const ip = cidr.replace("/" + mask, "");
	return { mask, ip };
}

export default findIp;