/**
 * Convert an array of decimal to an array of binary
 * @param {Array<String>} arrayDec -  [ '10', '27', '233', '17' ]
 * @returns {Array<String>} arrayBin - [ '00001010', '00011011', '11101001', '00010001' ]
 */
function convertArrayDecToArrayBin(arrayDec) {
	return arrayDec.map(
		e => parseInt(e).toString(2).padStart(8, "0"));
}

/**
 * Replace a character of a string by another with index
 * @param {string} string
 * @param index
 * @param replace
 * @returns {*}
 */
function replaceAt(string, index, replace) {
	return string.substring(0, index) + replace +

		string.substring(index + 1);
}

/**
 * Inverse 0 and 1 in all strings in a array
 * @param {String[]} arrayToInverse  Ex:[ '00001010', '00011011', '11101001', '00010001' ]
 * @returns {[]}  [ '11110101', '11100100', '00010110', '11101110' ]
 */
function inverseZeroAndOneCharForArray(arrayToInverse) {
	let arrayToInverseInv = [];
	for ( let i = 0 ; i <= arrayToInverse.length - 1 ; i++ ) {

		let a = arrayToInverse[i];

		for ( let x = 0 ; x < 8 ; x++ ) {
			if ( a[x] === "0" ) {
				// replace a[0] = 1
				a = replaceAt(a, x, "1");
			} else {
				a = replaceAt(a, x, "0");
			}
		}
		arrayToInverseInv[i] = a;
	}
	return arrayToInverseInv;
}

/**
 *
 * @param {String[]} array8Bits
 * @param {String[]} arrayMaskBin
 * @param operation - && or ||
 * @returns {string[]}
 */
function arrayBitwiseOperations(array8Bits, arrayMaskBin, operation) {
	let networkBin = ["", "", "", ""];
	for ( let i = 0 ; i < 4 ; i++ ) {
		for ( let j = 0 ; j < 8 ; j++ ) {

			switch ( operation ) {
				case "&&":
					if ( array8Bits[i][j] === "1" && arrayMaskBin[i][j] ===
						"1" ) {
						networkBin[i] = networkBin[i].concat("1");
					} else {
						networkBin[i] = networkBin[i].concat("0");
					}
					break;

				case "||":
					if ( array8Bits[i][j] === "1" || arrayMaskBin[i][j] ===
						"1" ) {
						networkBin[i] = networkBin[i].concat("1");
					} else {
						networkBin[i] = networkBin[i].concat("0");
					}
					break;
			}
		}
	}
	return networkBin;
}


export { convertArrayDecToArrayBin, inverseZeroAndOneCharForArray, replaceAt ,arrayBitwiseOperations};
